### Milo Serif Web

<button class="js-specimen-modal-trigger pull-right btn btn-red" data-font-class='milo-serif-web-text-normal'  data-font-name='Milo Serif Web'  data-target='.bs-example-modal-lg'  data-toggle='modal', type='button'>View Sample</button>
### Serif Normal
<style>
.serif-normal {
  font-family: MiloSerifWeb, TimesNewRoman, "Times New Roman", Times, Baskerville, Georgia, serif;
  font-style: normal;
  font-weight: normal;
  font-size: 36px;
  word-wrap: break-word;
}
.serif-medium {
  font-family: MiloSerifWeb, TimesNewRoman, 'Times New Roman', Times, Baskerville, Georgia, serif;
  font-size: 36px;
  font-weight: 700;
  word-wrap: break-word;
}
</style>

<div class="serif-normal">
  <div class="example">
   <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
   <span class="text-uppercase">abcdefghijklmnopqrstuvwxyz</span>
   <span>abcdefghijklmnopqrstuvwxyz</span> <br />
   <span>0123456789</span> <br />
   <span>!@#$%^&</span>
 </div>
</div>


```css
.serif-normal {
  font-family: MiloSerifWeb, TimesNewRoman, "Times New Roman", Times, Baskerville, Georgia, serif;
  font-style: normal;
  font-weight: normal;
}
```

<button class="js-specimen-modal-trigger pull-right btn btn-red" data-font-class='milo-serif-web-text-medium'  data-font-name='Milo Serif Web Medium'  data-target='.bs-example-modal-lg'  data-toggle='modal'  type='button'>View Sample</button>
### Serif Medium

<div class="serif-medium">
  <div class="example">
   <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
   <span class="text-uppercase">abcdefghijklmnopqrstuvwxyz</span>
   <span>abcdefghijklmnopqrstuvwxyz</span> <br />
   <span>0123456789</span> <br />
   <span>!@#$%^&</span>
 </div>
</div>


```css
.serif-medium {
  font-family: MiloSerifWeb, TimesNewRoman, "Times New Roman", Times, Baskerville, Georgia, serif;
  font-style: normal;
  font-weight: 700;
}
```


