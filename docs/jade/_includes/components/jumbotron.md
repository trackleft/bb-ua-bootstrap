<h1 id="jumbotron">Jumbotron</h1>
<p>A lightweight, flexible component that can optionally extend the entire viewport to showcase key content on your site.</p>
<div class="example" data-example-id="simple-jumbotron">
  <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
  <div class="jumbotron">
    <h1 class="margin-align-middle">Hello, world!</h1>
    <p>This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
    <p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a></p>
  </div>
</div>

```html
<div class="jumbotron">
<h1 class="margin-align-middle">Hello, world!</h1>
<p>...</p>
<p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a></p>
</div>
```
<p>To make the jumbotron full width, and without rounded corners, place it outside all <code>.container</code>s and instead add a <code>.container</code> within.</p>
```html
<div class="jumbotron">
<div class="container">
  ...
</div>
</div>
```

