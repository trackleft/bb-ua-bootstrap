<h1 id="breadcrumbs">Breadcrumbs</h1>

<p class="lead">Indicate the current page's location within a navigational hierarchy.</p>
<p>Separators are automatically added in CSS through <code>:before</code> and <code>content</code>.</p>
<div class="example" data-example-id="simple-breadcrumbs">
  <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
  <ol class="breadcrumb">
    <li class="active">Home</li>
  </ol>
  <ol class="breadcrumb">
    <li><a href="#">Home</a></li>
    <li class="active">Library</li>
  </ol>
  <ol class="breadcrumb">
    <li><a href="#">Home</a></li>
    <li><a href="#">Library</a></li>
    <li class="active">Data</li>
  </ol>
</div>

```html
<ol class="breadcrumb">
<li><a href="#">Home</a></li>
<li><a href="#">Library</a></li>
<li class="active">Data</li>
</ol>
```
